using FancyCalc;
using NUnit.Framework;

namespace FancyCalcTest.Tests
{
    public class Tests
    {
        [Test]
        public void AddTest()
        {
            FancyCalcEnguine calc = new FancyCalcEnguine();
            double expected = 5;
            double actual = calc.Add(2, 3);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void SubtractTest()
        {
            var calc = new FancyCalcEnguine();
            double expected = 0;
            double actual = calc.Subtract(1, 1);
            Assert.AreEqual(expected, actual);
        }

        [TestCase(3, 3, ExpectedResult = 9)]
        [TestCase(1, 0, ExpectedResult = 0)]
        public double MultiplyTest(int a, int b)
        {
            var calc = new FancyCalcEnguine();
            return calc.Multiply(a, b);
        }
        [Test]
        public void MultiplyTest2()
        {
            var calc = new FancyCalcEnguine();
            Assert.AreEqual(0, calc.Multiply(0, 9));
        }
    }
}